import webapp2

class Push200(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Push OK!')

class Push500(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Push Fail!')
	self.response.set_status(500)

application = webapp2.WSGIApplication([
    ('/push-200', Push200),
    ('/push-500', Push500),
], debug=True)
